---
layout: default
title: Android portfolio
type: postList
---
{% for post in site.posts %}

  {% if post.categories contains "android" %}

  {% include portfolio-entry.html %}

  {% endif %}

{% endfor %}
