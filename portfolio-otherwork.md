---
layout: default
title: Other works
type: postList
---
{% for post in site.posts %}

  {% if post.categories contains "otherwork" %}

  {% include portfolio-entry.html %}

  {% endif %}

{% endfor %}
