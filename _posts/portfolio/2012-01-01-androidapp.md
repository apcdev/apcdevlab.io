---
layout: post
title:  "Cablevision Movil"
categories: [android]
color: "#2196F3"
actioncolor: "#90CAF9"
androidtags: [Gingerbread, Listview, Linear layout]
techtags: [web services, multithreading, json, maps, authentication]
cardImage: "android/cable/cable_1.jpg"
devices: ["Galaxy S3","Nexus S", "Galaxy Nexus"]
images: []
---
Was a companion app for pay television company called Cablevision, as my first android
app, my job was to fix several bugs related to the **webservices** and user experience on the app.
Its main features were the following:
* Display the information of the channels, filter by type.
* Create **reminders** for watching a specific program
* View the user's account data
* Display a **map** for locating stores and use **gps** for setting up routes for getting to them
* Request prices from different packages
