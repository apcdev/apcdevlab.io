---
layout: post
title:  "Corpored"
categories: [android]
color: "#304FFE"
actioncolor: "#A3B1FF"
techtags: [web services, json, bluetooth, authentication]
androidtags: [Holoeverywhere, RelativeLayout, Ice cream Sandwich]
cardImage: "android/corpo/corpo_1.png"
images: ["android/corpo/corpo_2.png", "android/corpo/corpo_3.png", "android/corpo/corpo_4.png"]
devices: ["Galaxy Note", "Moto G"]
---
Was an point of sale app for a company called Corpored, I was in charge of developing the app from the start,
its core functions were the following:
* Capable of connecting to **bluetooth** printers in order to print receipts
* Functionality for allowing registration of new users and **secure login** for returning users.
* Allow users to visualize and manage their account data
* Allow users to view the historical data of the POS operations
* Allow users to make POS operations
