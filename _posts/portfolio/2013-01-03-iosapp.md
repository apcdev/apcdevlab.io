---
layout: post
title:  "CNSF citas"
categories: [ios]
color: "#2196F3"
actioncolor: "#4DABF5"
techtags: [web services, json, maps, testflight]
cardImage: "ios/cnsf/cnsf_1.png"
images: ["ios/cnsf/cnsf_2.png","ios/cnsf/cnsf_3.png","ios/cnsf/cnsf_4.png"]
devices: ["iPhone"]
---
Was an app for Mexico's insurance and surety bond regulator the Comision Nacional de Seguros y Fianzas (CNSF) and was
used for scheduling appointments for the insurance brokers.
Using a webservice made by me using **python** the app was able to achieve following:
* Allow the user to prefill the required data for scheduling appointment.
* Preview on **map** the locations of the different offices to schedule the appointments.
* Preview on a **custom calendar** the date availability for scheduling appointments
* Make appointments and visualize scheduled appointments
* Cypher through webservice interactions
