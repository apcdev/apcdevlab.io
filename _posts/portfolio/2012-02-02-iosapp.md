---
layout: post
title:  "Sky cotizador"
categories: [ios]
color: "#FF6D00"
actioncolor: "#FFD699"
iostags: [plists, modals, split view]
cardImage: "ios/sky/cot_1.png"
images: ["ios/sky/cot_2.png","ios/sky/cot_3.png","ios/sky/cot_4.png"]
devices: ["iPad"]
tabletLayout: true
---
Was an a **iPad** app for a pay television company named Sky and it was used for allowing potential clients to view their possible combos for acquiring a pay television plan. I developed this app with the following features:
* Allow visualization of predefined tv packages
* Allow visualization of generated packages according to filters: price and channel type
* Dynamic generation of packages according the business rules.
