---
layout: post
title:  "Simulador de manejo"
categories: [ios]
color: "#6B9CEC"
actioncolor: "#9BC1FF"
iostags: [AVPlayer, dispatch methods, plists]
cardImage: "ios/sim/sim_1.png"
images: ["ios/sim/sim_2.png","ios/sim/sim_3.png","ios/sim/sim_4.png"]
devices: ["iPad"]
tabletLayout: true
---
Was an a **iPad** app I did as freelancer for a company named Duplou and it was used for basic
training for new drivers for Pepsico company, I was responsible of creating the app, it's main features were the following:
* Display intro screens of app features
* Ran an interactive test where users using the **gyroscope** and **accelerometer** were able to do actions that should be done based on the background video within the tablet.
* At the end a result of the test was shown to the user, based of the assertivity of the interactions.
