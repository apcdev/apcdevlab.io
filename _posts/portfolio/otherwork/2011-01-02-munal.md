---
layout: post
title:  "Munal App"
categories: [otherwork]
color: "#2196F3"
actioncolor: "#90CAF9"
techtags: [qr reader,videos]
cardImage: "otherwork/munal/munal_1.png"
devices: ["Blackberry"]
images: ["otherwork/munal/munal_2.jpg","otherwork/munal/munal_3.jpg"]
oldDeviceLayout: true
---
Was a concept app I developed for Mexico's National Art Museum (MUNAL), it's main feature was to allow users to scan
qrs for getting further information related to the displayed art on the museum. After scanning with the camera, the app would display
more text, or videos related to the art.
It used **Zebra Crossing** library for QR scanning.
