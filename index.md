---
layout: default
title: About
type: aboutPage
---

<div class="about-row about-me">
  <div class = "about-skill-card mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-shadow--4dp" >
    <div class="about-me-image mdl-color--blueGray-100">
  	</div>
    <div class="about-me-detail">
      <h5>About me</h5>
      <div class="mdl-card__supporting-text ">
        I'm a software developer, who keeps looking forward to learning new technologies and seeks for new ways to improve myself
        and everything that work on. My main passion is the world around <b>mobile technologies</b> and to share with others the things
        that I've discovered during my jorney.
      </div>
      <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" href="assets/pdf/ResumeAlfonsoPalacios.pdf" target="_blank" >
        Resume
      </a>
    </div>

  </div>
</div>

<h5 class="about-title">Main skills</h5>

<div class="about-row">
  <div class = "about-skill-card mdl-cell mdl-cell--6-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-shadow--4dp" >

    <div class="about-skill-image mdl-color--teal-100">
    	<i class="fas fa-mobile-alt"></i>
  	</div>

  	<div class="about-skill-detail">
      <div>Mobile Development</div>
      <div class="mdl-card__supporting-text">
        The area where I'm specialized the most, my whole professional path has been based on this.
      </div>
      <div class="mdl-layout--small-screen-only">
        <a class="portfolio-android-button mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" href="/portfolio-android.html" ><i class="fab fa-android"></i> My Android Portfolio</a>
        <br />
        <a class="portfolio-ios-button mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" href="/portfolio-ios.html" ><i class="fab fa-apple"></i> My iOS Portfolio</a>
      </div>
      <div class="mdl-layout--large-screen-only">
        <span class="mdl-chip">
            <span class="mdl-chip__text">iOS</span>
        </span>
        <span class="mdl-chip">
            <span class="mdl-chip__text">Android</span>
        </span>
        <span class="mdl-chip">
            <span class="mdl-chip__text">PhoneGap</span>
        </span>
        <span class="mdl-chip">
            <span class="mdl-chip__text">Blackberry</span>
        </span>
        <span class="mdl-chip">
            <span class="mdl-chip__text">Java ME</span>
        </span>
      </div>


  	</div>

  </div>

  <div class = "about-skill-card mdl-cell mdl-cell--6-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-shadow--4dp" >

    <div class="about-skill-image mdl-color--indigo-100">
    	<i class="fas fa-desktop"></i>
  	</div>

  	<div class="about-skill-detail">
      <div>Web Development</div>
      <div class="mdl-card__supporting-text">
        The partner area that traveled with me during my mobile journeys, sometimes a CMS or a webservice is needed in order
        to nurture the contents of an app.
      </div>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Django</span>
      </span>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Rails</span>
      </span>
      <span class="mdl-chip">
          <span class="mdl-chip__text">ASP MVC</span>
      </span>
  	</div>

  </div>
</div>

<div class="about-row">
  <div class = "about-skill-card mdl-cell mdl-cell--6-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-shadow--4dp" >

    <div class="about-skill-image mdl-color--red-100">
    	<i class="fas fa-pencil-ruler"></i>
  	</div>

  	<div class="about-skill-detail">
      <div>UI Design</div>
      <div class="mdl-card__supporting-text">
        An interesting area, I always look for ways to improve interfaces and make them unique but at
        the same time easy to use.
      </div>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Sketch</span>
      </span>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Figma</span>
      </span>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Pencil</span>
      </span>
  	</div>

  </div>

  <div class = "about-skill-card mdl-cell mdl-cell--6-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-shadow--4dp" >

    <div class="about-skill-image mdl-color--blue-100">
    	<i class="fas fa-users"></i>
  	</div>

  	<div class="about-skill-detail">
      <div>Scrum</div>
      <div class="mdl-card__supporting-text">
        A MUST practice to follow up in the projects, once you start using Scrum, you can't go back.
      </div>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Jira</span>
      </span>
      <span class="mdl-chip">
          <span class="mdl-chip__text">Taiga</span>
      </span>
  	</div>

  </div>
</div>

<h5 class="about-title">Programming languages</h5>
<div class="about-card mdl-shadow--4dp">

  <h6>Main languages</h6>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Swift</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Objective C</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Java</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Python</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Ruby</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Javascript</span>
  </span>

  <h6>Secondary languages</h6>
  <span class="mdl-chip">
      <span class="mdl-chip__text">C#</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Scala</span>
  </span>
  <span class="mdl-chip">
      <span class="mdl-chip__text">Erlang</span>
  </span>

</div>

<h5 class="about-title">Work experience</h5>

<div class="about-card mdl-shadow--4dp">

  <ul class="about-timeline demo-list-two mdl-list">
    <li class="mdl-list__item mdl-list__item--two-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>Globant</span>
        <span class="mdl-list__item-sub-title">2018 - Current</span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--two-line">

      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>Comisión Nacional de Seguros y de Fianzas</span>
        <span class="mdl-list__item-sub-title">2013 - 2018</span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--two-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>Grupo Corpored</span>
        <span class="mdl-list__item-sub-title">2011 - 2012</span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--two-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>SKY México</span>
        <span class="mdl-list__item-sub-title">2011</span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--two-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>LATBC</span>
        <span class="mdl-list__item-sub-title">2010 - 2011</span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--two-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>Blue Label México</span>
        <span class="mdl-list__item-sub-title">2009 - 2010</span>
      </span>
    </li>
  </ul>

</div>

<h5 class="about-title">Education</h5>

<div class="about-card mdl-shadow--4dp">

  <ul class="about-timeline demo-list-two mdl-list">
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>
          <b>Scrum master certified</b>
        </span>
        <div>Scrum Alliance Mexico (2016)</div>
        <a href="https://www.scrumalliance.org/community/profile/apalaciosc" target="_blank">View certificate</a>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>
          <b>Programming mobile course for Android</b>
        </span>
        <div>Coursera- University of Maryland (2015)</div>
        <a href="https://www.coursera.org/account/accomplishments/certificate/E9YUMWCWUZ" target="_blank">View certificate</a>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>
          <b>Diplomate in mobile technologies</b>
        </span>
        <div>Instituto Tecnologico y de Estudios Superiores de Monterrey (2012)</div>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <div class="timeline-bubble"></div>
        <span>
          <b>BSc, Computer Science</b>
        </span>
        <div>Instituto Tecnologico y de Estudios Superiores de Monterrey (2005 - 2010)</div>
      </span>
    </li>

  </ul>
</div>
